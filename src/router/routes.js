
const routes = [
  {path: '/', component: () => import('pages/Index.vue')},
  {path: '/agendamento', component: () => import('pages/Agendamento.vue')},
  {path: '/forgot', component: () => import('pages/ForgotPassword.vue')},
  {path: '/login', component: () => import('pages/Login.vue')},
  {path: '/cadastro', component: () => import('pages/Cadastro.vue')},
  {path: '/home', component: () => import('pages/Home.vue')},
  {path: '/servicos', component: () => import('pages/Servicos.vue')},
  {path: '/profile', component: () => import('pages/Profile.vue')}

]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
